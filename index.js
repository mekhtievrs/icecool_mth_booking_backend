const express = require('express')
const app = express()
const port = 3001

let tours = [];
const fs = require("fs");
fs.readFile("./tours.json", "utf8", (error, data) => {
    if (error) {
        console.log(error);
        return;
    }
    tours = JSON.parse(data)
});

const getById = (id) => {
    return tours.filter((item) => item.id === parseInt(id))[0];
}

const getPaginatedTours = (req) => {
    const page = req.query.page || 1;
    const perPage = req.query.perPage || 10;
    const from = page === 1 ? 0 : (page - 1) * perPage;
    const total = tours.length;
    const to = (page * perPage) > total ? total : (page * perPage);
    const data = tours.slice(from, to)

    const meta = {
        current_page: page,
        from: from - 1,
        to,
        per_page: perPage,
        total: total,
    }
    return {
        data,
        meta,
    };
}

app.get('/tours', (req, res) => {
    const response = getPaginatedTours(req)
    res.send(response);
})

app.get('/tours/:tourId', (req, res) => {
    const tour = getById(req.params.tourId)
    if (!tour) {
        res.status(404).send({message: 'Not found'});
    }
    res.send({
        "data": tour
    });
})

app.get('/user/:userId/tours', (req, res) => {
    const response = getPaginatedTours(req)
    res.send(response);
})

app.get('/user/:userId/tours/:tourId', (req, res) => {
    const tour = getById(req.params.tourId)
    if (!tour) {
        res.status(404).send({message: 'Not found'});
    }
    res.send({
        "data": tour
    });
})

app.listen(port, () => {
    console.log(`App listening on port ${port}`)
})